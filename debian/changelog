rust-libz-sys (1.1.20-1+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Tue, 11 Mar 2025 16:22:53 +0000

rust-libz-sys (1.1.20-1) unstable; urgency=medium

  * Package libz-sys 1.1.20 from crates.io using debcargo 2.7.1

  [ Blair Noctis ]
  * Team upload.
  * Package libz-sys 1.1.12 from crates.io using debcargo 2.6.0

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Sun, 13 Oct 2024 16:43:30 +0200

rust-libz-sys (1.1.8-2+apertis1) apertis; urgency=medium

  * Switch component from development to target to comply with Apertis
    license policy.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Thu, 16 Jan 2025 17:19:35 +0100

rust-libz-sys (1.1.8-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 08 Apr 2023 00:13:07 +0000

rust-libz-sys (1.1.8-2) unstable; urgency=medium

  * Team upload.
  * Package libz-sys 1.1.8 from crates.io using debcargo 2.5.0
  * Upload to unstable.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 18 Oct 2022 15:59:08 +0000

rust-libz-sys (1.1.8-1) experimental; urgency=medium

  * Team upload.
  * Package libz-sys 1.1.8 from crates.io using debcargo 2.5.0
  * Closes: #972361 - zlib-ng dependency and feature dropped

 -- Fabian Gruenbichler <f.gruenbichler@proxmox.com>  Tue, 27 Sep 2022 20:32:33 -0400

rust-libz-sys (1.1.2-3apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 20:17:52 +0000

rust-libz-sys (1.1.2-3) unstable; urgency=medium

  * Team upload.
  * Package libz-sys 1.1.2 from crates.io using debcargo 2.4.3
  * Mark all features autopkgtest as broken, it tries to build
    a missing embedded copy of zlib.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 10 Dec 2020 18:01:47 +0000

rust-libz-sys (1.1.2-2) unstable; urgency=medium

  * Team upload.
  * Source upload
  * Package libz-sys 1.1.2 from crates.io using debcargo 2.4.3

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 18 Oct 2020 11:16:01 +0200

rust-libz-sys (1.1.2-1) unstable; urgency=medium

  * Source upload
  * Package libz-sys 1.1.2 from crates.io using debcargo 2.4.3

 -- Ximin Luo <infinity0@debian.org>  Thu, 01 Oct 2020 23:25:37 +0100

rust-libz-sys (1.0.25-1) unstable; urgency=medium

  * Team upload.
  * Source upload
  * Package libz-sys 1.0.25 from crates.io using debcargo 2.2.9

  [ Ximin Luo ]
  * Source upload
  * Package libz-sys 1.0.25 from crates.io using debcargo 2.2.8

 -- Matt Kraai <kraai@debian.org>  Sat, 08 Dec 2018 13:27:55 -0800

rust-libz-sys (1.0.18-1) unstable; urgency=medium

  * Source upload
  * Package libz-sys 1.0.18 from crates.io using debcargo 2.2.5

 -- Ximin Luo <infinity0@debian.org>  Fri, 03 Aug 2018 05:11:26 -0700
